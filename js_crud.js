let nestedObj = {
    obj2: {
        arr1: [1, 2, 4, {
            'nested value': [5,6,7]
        }]
    },
    obj3: {
        obj4: {
            obj5: {
                'nested object':'This is a nested object value'
            }
        },
        arr2:[true, 'Value inside arr2']
    }
}
// Q1) 1. Access the value of 'nested value' key.
var a = nestedObj.obj2.arr1[3]["nested value"]
console.log(a)

// Q2) Access the value of 7
var b = nestedObj.obj2.arr1[3]["nested value"][2]
console.log(b)

// Q3) Access the value of obj5 key
var c = nestedObj.obj3.obj4.obj5
console.log(c)

// Q4) Access the value of 'nested object' key.
var d = nestedObj.obj3.obj4.obj5["nested object"]
console.log(d)

// Q5) Access the value of 'value inside arr2'
var e = nestedObj.obj3.arr2[1]
console.log(e)

